﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanConsoleApp
{
  public class Number
    {
        private int _value;

        public Number(int number)
        {
            this._value = number;
            
        }

        public string ToRoman()
        {
            return InternalRoman(_value);
            
        }
        private  string InternalRoman(int number)
        {
            if ((number < 0) || (number > 3000)) throw new ArgumentOutOfRangeException("insert value between 1 and 3000");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + InternalRoman(number - 1000);
            if (number >= 900) return "CM" + InternalRoman(number - 900);
            if (number >= 500) return "D" + InternalRoman(number - 500);
            if (number >= 400) return "CD" + InternalRoman(number - 400);
            if (number >= 100) return "C" + InternalRoman(number - 100);
            if (number >= 90) return "XC" + InternalRoman(number - 90);
            if (number >= 50) return "L" + InternalRoman(number - 50);
            if (number >= 40) return "XL" + InternalRoman(number - 40);
            if (number >= 10) return "X" + InternalRoman(number - 10);
            if (number >= 9) return "IX" + InternalRoman(number - 9);
            if (number >= 5) return "V" + InternalRoman(number - 5);
            if (number >= 4) return "IV" + InternalRoman(number - 4);
            if (number >= 1) return "I" + InternalRoman(number - 1);
            return string.Empty;
        }
    }
}
