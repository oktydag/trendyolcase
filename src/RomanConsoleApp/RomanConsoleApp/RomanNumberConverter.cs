﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanConsoleApp
{
    public class RomanNumberConverter
    {
        public static void Main(string[] args)
        {

            while (true)
            {
                Console.WriteLine("Enter a number");
                string input = Console.ReadLine();
                var number = IsNumeric(input) ? Convert.ToInt32(input) : -1;

                if (IsValidNumber(number))
                {
                    //Number romanNumber = new Number(number);
                    Numeric numeric = new Numeric(number);
                    Console.WriteLine(number.ToString() + " --> " + numeric.ToRoman() + "\n");
                }
                else
                {
                    Console.WriteLine("Enter a number that is between 1 and 3000" + "\n");
                }

                SeperateEachResult();
            }
        }

        private static void SeperateEachResult()
        {
            Console.WriteLine("**************************************");
        }

        private static bool IsValidNumber(int number)
        {
            if (number < 0 || number > 3000) return false;
            return true;
        }
        private static bool IsNumeric(string checkedNumber)
        {
            int output;
            return int.TryParse(checkedNumber, out output);
        }
    }
}
