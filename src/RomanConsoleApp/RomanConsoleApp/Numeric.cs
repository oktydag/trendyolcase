﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanConsoleApp
{
    public class Numeric
    {
        private int _value;

        public Numeric(int number)
        {
            this._value = number;

        }

        public string ToRoman()
        {
            return InternalOfConverterRoman(_value);

        }
        private static string InternalOfConverterRoman(int number)
        {
            Dictionary<string, int> valueOfRomanNumbers = new Dictionary<string, int>()
        {
            {"M", 1000},
            {"CM", 900},
            {"D", 500},
            {"CD", 400},
            {"C", 100},
            {"XC", 90},
            {"L", 50},
            {"XL", 40},
            {"X", 10},
            {"IX", 9},
            {"V", 5},
            {"IV", 4},
            {"I", 1},
        };
            if ((number < 0) || (number > 3000)) throw new ArgumentOutOfRangeException("insert value between 1 and 3000"); // Test case expected

            string resultRomanNumber = string.Empty;

            for (int i = 0; number > 0; i++)
            {
                while (valueOfRomanNumbers.ElementAt(i).Value <= number)
                {
                    resultRomanNumber += valueOfRomanNumbers.ElementAt(i).Key;
                    number -= valueOfRomanNumbers.ElementAt(i).Value;
                }
            }
            return resultRomanNumber;
        }
    }
}
