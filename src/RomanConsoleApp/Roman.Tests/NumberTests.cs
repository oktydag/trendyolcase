﻿using System;
using RomanConsoleApp;
using NUnit.Framework;


namespace Roman.Tests
{
    [TestFixture]
    public class NumberTests
    {
        [TestCase(10, "X")]
        [TestCase(4, "IV")]
        [TestCase(11, "XI")]
        [TestCase(89, "LXXXIX")]
        [TestCase(456, "CDLVI")]
        [TestCase(990, "CMXC")]
        [TestCase(1244, "MCCXLIV")]
        [TestCase(2999, "MMCMXCIX")]
        public void When_Number_Is_Given_Should_Return_Roman_Number(int number, string expected)
        {
            Numeric testNumber = new Numeric(number);
            // Number testNumber = new Number(number); // SecondWay
            var result = testNumber.ToRoman();
            Assert.AreEqual(result, expected);
        }

        [Test]
        public void When_Given_Number_Less_Then_Zero_Should_Throw_Exception()
        {
            Numeric testNumber = new Numeric(-100);
            //  Number testNumber = new Number(-100); // SecondWay
            Assert.Throws<ArgumentOutOfRangeException>(() => testNumber.ToRoman());
        }

        [Test]
        public void When_Given_Number_EqualTo_Zero_Should_Return_Be_Null()
        {
            Numeric testNumber = new Numeric(0);
            // Number testNumber = new Number(0); // SecondWay
            var result = testNumber.ToRoman();
            Assert.AreEqual(result, string.Empty);
        }

        [Test]
        public void When_Given_Number_Greater_Then_3000_Should_Throw_Exception()
        {
            Numeric testNumber = new Numeric(4021);
            // Number testNumber = new Number(4021);  // SecondWay
            Assert.Throws<ArgumentOutOfRangeException>(() => testNumber.ToRoman());
        }
    }

}
