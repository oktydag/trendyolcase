﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrendCart.Model;
//using TrendCart.Tests.Creators;

namespace TrendCart.Tests
{
    [TestFixture]
    public class TrendCartTest
    {
        [Test]
        public void Adding_Product_To_Cart()
        {
            // Act
            Category category = new Category(1, "food");
            Product apple = new Product("Apple", 100.0, category);

            ShoppingCart cart = new ShoppingCart();

            // Arrange
            cart.AddItem(apple, 3);
            cart.AddItem(apple, 4);

            // Assert
            Assert.AreEqual(cart.Products.Count, 1);
            Assert.AreEqual(cart.Products[apple], 7);
        }

        //[Test]
        //public void Apply_Discount_Test()
        //{
        //    Category category = new Category(1, "food");

        //    ShoppingCart cart = ShoppingCartCreator.Create();

        //    Campaign discount1 = new Campaign(category, 20.0, 3, DiscountType.Rate);
        //    Campaign discount2 = new Campaign(category, 50.0, 5, DiscountType.Rate);
        //    Campaign discount3 = new Campaign(category, 5.0, 5, DiscountType.Amount);

        //    cart.ApplyDiscounts(discount1, discount2, discount3);
        //}

        [Test]
        public void Apply_Coupon_to_Cart()
        {
            Coupon coupon = new Coupon(100, 10, DiscountType.Rate);
            ShoppingCart cart = new ShoppingCart();

            cart.ApplyCoupon(coupon);

            Assert.AreEqual(90, coupon.CartAmount);
        }
    }


}
