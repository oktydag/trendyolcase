﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendCart.Model
{
    public class ShoppingCart
    {
        public Dictionary<Product, int> Products { get; private set; }
        private const double minAcceptableAmountForCoupon = 100d;

        public ShoppingCart()
        {
            Products = new Dictionary<Product, int>();
        }

        public void AddItem(Product product, int quantity)
        {
            if (Products.ContainsKey(product))
                Products[product] += quantity;
            else
                Products.Add(product, quantity);
        }

        public void ApplyDiscounts(params Campaign[] campaigns)
        {
            var campaignsByCategoryGroup = campaigns.GroupBy(x => x.Category, (key, g) => new { Category = key, Campaign = g, CampaingCount = g.Count() });

            var productCategories = Products.GroupBy(x => x.Key.Category, (key, g) => new { Category = key, ProductCount = g.Sum(d => d.Value) });

            var availableDiscounts = new List<Campaign>();


            foreach (var campaign in campaigns)
            {
                var cartClone = Products.Select(item => (Product)item.Key.Clone()).ToList();
                var currentProductCategory = productCategories.FirstOrDefault(x => x.Category.Id == campaign.Category.Id);

                if (currentProductCategory.ProductCount > campaign.ItemCount)
                    availableDiscounts.Add(campaign);
            }

            // TODO : available Discounts will be applied.
        }

        public void ApplyCoupon(Coupon coupon)
        {
            if (coupon.CartAmount < minAcceptableAmountForCoupon) return;
            if (coupon.DiscountType == DiscountType.Rate)
            {
                coupon.CartAmount -= (coupon.CartAmount * coupon.CouponDiscountPersentage) / 100;
            }
        }

    }
}
