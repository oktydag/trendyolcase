﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendCart.Model
{
    public class Campaign
    {
        public Category Category { get; private set; }
        public double Amount { get; private set; }
        public int ItemCount { get; set; }
        public DiscountType DiscountType { get; private set; }

        public Campaign(Category category, double amount, int itemCount, DiscountType discountType)
        {
            Category = category;
            Amount = amount;
            DiscountType = discountType;
            ItemCount = itemCount;
        }

    }
}
