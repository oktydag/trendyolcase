﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendCart.Model
{
    public class Coupon
    {

        public double CartAmount { get; set; }
        public double CouponDiscountPersentage { get; private set; }
        public DiscountType DiscountType { get; set; }

        public Coupon(double amount, double couponDiscountPersentage, DiscountType discountType)
        {
            CartAmount = amount;
            CouponDiscountPersentage = couponDiscountPersentage;
            DiscountType = discountType;
        }



    }
}
