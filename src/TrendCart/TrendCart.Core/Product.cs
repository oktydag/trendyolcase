﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendCart.Model
{
    public class Product : ICloneable
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public double Price { get; private set; }
        public Category Category { get; private set; }
        public Product(string name, double price, Category category)
        {
            Id = new Guid();
            Name = name;
            Price = price;
            Category = category;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
