﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendCart.Model
{
    public class Category
    {
        public int Id { get; private set; }

        public string Title { get; private set; }
        public Category Parent { get; set; }

        public Category(int id, string title)
        {
            this.Title = title;
            Id = id;
        }

    }
}
